var classpars_transformation =
[
    [ "parsTransformation", "classpars_transformation.html#a237c41f278cd235984d01c3bc6e957e7", null ],
    [ "~parsTransformation", "classpars_transformation.html#a1c637e91858b0c5397f640b8220fec44", null ],
    [ "GetCombine", "classpars_transformation.html#a7e3e36b80c38d7256fe78f21f0aea1a5", null ],
    [ "GetFlipVert", "classpars_transformation.html#a54d9460c75017b17825fb2d423d4996d", null ],
    [ "GetRotate90", "classpars_transformation.html#aa5d8d13298bdd4ed63f9d682e865bef0", null ],
    [ "SetCombine", "classpars_transformation.html#a612e0030fd85ad5dfd97b1e76e589064", null ],
    [ "SetFlipVert", "classpars_transformation.html#a47c80db1e14fc819f2e06c6ccd5cda3a", null ],
    [ "SetRotate90", "classpars_transformation.html#a1e9bbadabad7bb7a2604e8a6f99af7d6", null ],
    [ "fCombine", "classpars_transformation.html#a108db04a328569f4d97df02c4ed0e93a", null ],
    [ "fFlipVert", "classpars_transformation.html#ab0adeeb2a0ee4811d52e12f397ee69f6", null ],
    [ "fRotate90", "classpars_transformation.html#a82857abc582a5e9dde88f042794870b4", null ]
];