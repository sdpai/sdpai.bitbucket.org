var classtif_header =
[
    [ "tifHeader", "classtif_header.html#a0e4a680b8f57d08abb9f29f26499dcd8", null ],
    [ "bitsPerSample", "classtif_header.html#ae0ee7f42d345eb32da8fe7c4fa00e558", null ],
    [ "byte0_1", "classtif_header.html#a57d4a61d4a761aa9112d037f9b3ec833", null ],
    [ "byte2_3", "classtif_header.html#ac213713e2af1a0d3576f6ab7ab220dca", null ],
    [ "byte4_7", "classtif_header.html#a931ad202335b78fcd783e7fc5c44136a", null ],
    [ "fmt", "classtif_header.html#a8f1049e7ff678b8caa0c2cf716fefec1", null ],
    [ "imageLength", "classtif_header.html#a69cbec38243b7a2308b49bf4b2c50093", null ],
    [ "imageWidth", "classtif_header.html#a66abfe9d2e49dc3a4f737ff8f2b7276f", null ],
    [ "maxSampleValue", "classtif_header.html#a6f9ada762918eb0c555ff1839f279432", null ],
    [ "numberOfDirectoryEntries", "classtif_header.html#a1273162fc3060a60fe45631438ed967b", null ],
    [ "samplesPerPixel", "classtif_header.html#a4f74cb1d8c6f70f10bb581f95bdd5d41", null ],
    [ "scanLine", "classtif_header.html#a0538427c4cecebc5bb84fee33c79ac8e", null ],
    [ "switchByte", "classtif_header.html#a11cfcad93368b08e46f069bdbc8b45f7", null ]
];