var classsd_cluster_char =
[
    [ "sdClusterChar", "classsd_cluster_char.html#a0d9b83cd4e3050a9ba8a36e314de7858", null ],
    [ "GetBorder", "classsd_cluster_char.html#a74caa7d600576d17a01ff06d517c5cf9", null ],
    [ "GetScan", "classsd_cluster_char.html#ac114e71d0ec456b4bffa287a830e0d94", null ],
    [ "IsNormalCluster", "classsd_cluster_char.html#a44637d0876859fdf66a61dd0dd60f0e6", null ],
    [ "SetBorder", "classsd_cluster_char.html#a24606245df6b0a2e76c0e6b1513f1153", null ],
    [ "SetScan", "classsd_cluster_char.html#ae9a065092798c40df22c999d23fb1035", null ],
    [ "SetType", "classsd_cluster_char.html#ae93eb8044fd73fbf3d6607f26b370eeb", null ],
    [ "aveamp", "classsd_cluster_char.html#a8b940abc516eedf6fec92c6c0a0adf98", null ],
    [ "colmax", "classsd_cluster_char.html#a581db69069cb473217aba917ab94e38a", null ],
    [ "colmin", "classsd_cluster_char.html#a25a4dc720b0e52876c4e40c8b0b0c9c4", null ],
    [ "duration", "classsd_cluster_char.html#a711527b50d976b2aca54eb2e4ed41cf8", null ],
    [ "maxamp", "classsd_cluster_char.html#afdc18618a91df0cfbd645bcefd52c6d0", null ],
    [ "numpixels", "classsd_cluster_char.html#abea24cdf37f57a3a40c1cb04bd99c5c6", null ],
    [ "rowmax", "classsd_cluster_char.html#ae137b2f131d7b1304c0f10009b5be931", null ],
    [ "rowmin", "classsd_cluster_char.html#ac3f07f31e9264c76ece4b9b8acff0c47", null ],
    [ "size", "classsd_cluster_char.html#a80f194d14ed7dae2e37b5488ed053547", null ],
    [ "type", "classsd_cluster_char.html#a33e3110a9529e12ae1e5bfcf982e0221", null ],
    [ "xhm1", "classsd_cluster_char.html#aa2573355d6bea624af76d1c5eabdfeb7", null ],
    [ "xhm2", "classsd_cluster_char.html#a0d60f5d7f72169bdfec6f5819b0985d6", null ],
    [ "yhm1", "classsd_cluster_char.html#a7730e4e9b26043460503c649e4017cf9", null ],
    [ "yhm2", "classsd_cluster_char.html#a80ee6f8213da1ddfbf526edfeb43dcf4", null ]
];