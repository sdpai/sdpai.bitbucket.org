var searchData=
[
  ['main',['main',['../sd_exe_8cpp.html#aca86eb835c26cd01b8abc4a4154a348e',1,'sdExe.cpp']]],
  ['main_2edox',['main.dox',['../main_8dox.html',1,'']]],
  ['makeclustersplitting',['MakeClusterSplitting',['../class_s_d_cluster_splitting_manager.html#a02bffdc5c30bdce7b48e7428babe131f',1,'SDClusterSplittingManager::MakeClusterSplitting()'],['../class_s_d_splitting_algo_base.html#a5c60557d46266bd3c166e0781993d33b',1,'SDSplittingAlgoBase::MakeClusterSplitting()'],['../class_s_d_splitting_algo_with_two_maximum_on_x.html#adf863281566d062b3a6524178fe8e9c1',1,'SDSplittingAlgoWithTwoMaximumOnX::MakeClusterSplitting()']]],
  ['maxadc',['maxAdc',['../_s_d_f0_base_8h.html#a87c5e9d46a76e46ac2cd33fa9f8c1c82',1,'SDF0Base.h']]],
  ['maxamp',['maxamp',['../classsd_cluster_char.html#afdc18618a91df0cfbd645bcefd52c6d0',1,'sdClusterChar']]],
  ['maxsamplevalue',['maxSampleValue',['../classtif_header.html#a6f9ada762918eb0c555ff1839f279432',1,'tifHeader']]],
  ['meanfilter',['MeanFilter',['../class_s_d_utils.html#a307d8c5e199320ca69e8a96db08a47db',1,'SDUtils::MeanFilter(TH2 *hin, UInt_t wx=1, UInt_t wy=1, TH2 *hout=0)'],['../class_s_d_utils.html#a00658d98857cc16cb790c28987d7e041',1,'SDUtils::MeanFilter(TH2 *hin, TString sopt, TH2 *hout=0)'],['../class_s_d_utils.html#ad3b057043dafdc97ffe6591f08b1ea08',1,'SDUtils::MeanFilter(TH1 *hin, UInt_t w=1, TH1 *hout=0, Int_t pri=0)']]],
  ['medianfilter',['MedianFilter',['../class_s_d_utils.html#a3a516b29ae3b47664cbf3f5e0d8ecb7c',1,'SDUtils::MedianFilter(TH2 *hin, UInt_t window=1, TH2 *hout=0)'],['../class_s_d_utils.html#a15c146d964012e2b9e96f8eb067d2da9',1,'SDUtils::MedianFilter(TH2 *hin, TString sopt, TH2 *hout=0)']]],
  ['mergeclusterfinders',['MergeClusterFinders',['../class_s_d_cluster_finder_base.html#ad92318ca54d8adc0d94c95cec6afc253',1,'SDClusterFinderBase::MergeClusterFinders()'],['../class_s_d_cluster_finder_std_one.html#ad5aa763456c08491c302637e5b65baa7',1,'SDClusterFinderStdOne::MergeClusterFinders()']]],
  ['minduration',['minduration',['../classpars_cluster_finder.html#a12d9a94675b4307049772de9cf5be141',1,'parsClusterFinder']]],
  ['mindurationinpxs',['mindurationInPxs',['../classpars_cluster_finder.html#a7c07dbcb139bdee0a4ab4c343a31aef8',1,'parsClusterFinder']]],
  ['minpixels',['minpixels',['../classpars_cluster_finder.html#a88fd7f859055dcfce5cfc55165c61e40',1,'parsClusterFinder']]],
  ['minsize',['minsize',['../classpars_cluster_finder.html#a3d6fb89db3539c676fb4e2585179b287',1,'parsClusterFinder']]],
  ['minsizeinpxs',['minsizeInPxs',['../classpars_cluster_finder.html#a9230b45b349d82d5cc4a81869fa020a7',1,'parsClusterFinder']]],
  ['movehiststolist',['moveHistsToList',['../class_s_d_aliases.html#a0039cdc12bae8b4b120081e9494e884d',1,'SDAliases']]],
  ['multiply',['multiply',['../class_s_d_aliases.html#aaeba14106217a1d15de8786e4ad6a918',1,'SDAliases::multiply(TArrayD *ar, const double c)'],['../class_s_d_aliases.html#aa341fa9d5d5d3968990025727f6141dc',1,'SDAliases::multiply(int n, double *ar, const double c)']]]
];
