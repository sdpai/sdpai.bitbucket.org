var classpars_cluster_finder =
[
    [ "parsClusterFinder", "classpars_cluster_finder.html#a954778247f1b3d8b6db42aa1bcfc7095", null ],
    [ "~parsClusterFinder", "classpars_cluster_finder.html#aa839e6b02ff5a638c6114664d8de59a1", null ],
    [ "CalculateSizesInPixels", "classpars_cluster_finder.html#afc18c566bfff86d283dcd8174adfa0cd", null ],
    [ "drawing", "classpars_cluster_finder.html#a0526201083f93294719ef1e7a23556c8", null ],
    [ "minduration", "classpars_cluster_finder.html#a12d9a94675b4307049772de9cf5be141", null ],
    [ "mindurationInPxs", "classpars_cluster_finder.html#a7c07dbcb139bdee0a4ab4c343a31aef8", null ],
    [ "minpixels", "classpars_cluster_finder.html#a88fd7f859055dcfce5cfc55165c61e40", null ],
    [ "minsize", "classpars_cluster_finder.html#a3d6fb89db3539c676fb4e2585179b287", null ],
    [ "minsizeInPxs", "classpars_cluster_finder.html#a9230b45b349d82d5cc4a81869fa020a7", null ],
    [ "name", "classpars_cluster_finder.html#a600d115cf747e94baa0c5a97432db6a6", null ],
    [ "seed", "classpars_cluster_finder.html#aa2f19668898e5ec6e333676c8fe31c0a", null ],
    [ "spacescale", "classpars_cluster_finder.html#a027ad7b169a9ffefd6a52959551851b5", null ],
    [ "timescale", "classpars_cluster_finder.html#a5f24308b9bfa50ab709152a76f7363dc", null ]
];