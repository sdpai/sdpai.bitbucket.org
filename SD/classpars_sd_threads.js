var classpars_sd_threads =
[
    [ "parsSdThreads", "classpars_sd_threads.html#acc3d5bf2b822a7e53516039fe30a437d", null ],
    [ "~parsSdThreads", "classpars_sd_threads.html#a15636432e50efb0133d49ee744c299c2", null ],
    [ "ClassDef", "classpars_sd_threads.html#acd90e40c8b34945555ebc1306f22d9fd", null ],
    [ "GetKeep", "classpars_sd_threads.html#aa9079b47eee5197398eb0795c4241e3a", null ],
    [ "GetMaxThreads", "classpars_sd_threads.html#a426303743572933f3660925fade97662", null ],
    [ "GetNcpus", "classpars_sd_threads.html#af41adcbaa77ab9408a0cbfa76653c9b9", null ],
    [ "IsMultiThreadingPossible", "classpars_sd_threads.html#a9e3a0233f9dde9638b81005b4df7ac8a", null ],
    [ "SetKeep", "classpars_sd_threads.html#a17271aea128a8c02cc914d2b4521c6f2", null ],
    [ "SetMaxThreads", "classpars_sd_threads.html#a2036172b86570cf01f95c6cb4c2ba0ac", null ],
    [ "fKeep", "classpars_sd_threads.html#a03ed54f880f1da10b3908cb01afb76c2", null ],
    [ "fMaxThreads", "classpars_sd_threads.html#a12a2571be52a5b254a01b6845f83ec57", null ],
    [ "fNcpus", "classpars_sd_threads.html#a4677748d487b8f37ba87495e34116740", null ],
    [ "fSysInfo", "classpars_sd_threads.html#a342ce2f01fffd336c8564393dc363830", null ]
];