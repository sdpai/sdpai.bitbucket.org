var classpars_sd_scan =
[
    [ "parsSdScan", "classpars_sd_scan.html#a7060238376a8c8fa93cce5e4f9efab5d", null ],
    [ "~parsSdScan", "classpars_sd_scan.html#af677794154fe17126e6d218f81c511f8", null ],
    [ "GetMaxNClusts", "classpars_sd_scan.html#a4f10529091165c51e0393e5d1b16731f", null ],
    [ "GetMaxRelAmp", "classpars_sd_scan.html#acbd32edfe6adbbecad0c89da1c10471d", null ],
    [ "GetMinStep", "classpars_sd_scan.html#a8e342aa969ad56b36f4563ceca71b081", null ],
    [ "GetNpScan", "classpars_sd_scan.html#a1debbb2f2e94517f308a3119251358a1", null ],
    [ "SetMaxNClusts", "classpars_sd_scan.html#ac959836422ba2bee53399afcba1cb2b6", null ],
    [ "SetMaxRelAmp", "classpars_sd_scan.html#aa5d726b5e1fe52cfa57f7240e56fa3ba", null ],
    [ "SetMinStep", "classpars_sd_scan.html#a31abcbbfb730738e0f3e4f8bd18b9c79", null ],
    [ "SetNpScan", "classpars_sd_scan.html#a65d6647859931df916da89d2fe712cd9", null ],
    [ "fMaxNClusts", "classpars_sd_scan.html#a56f15dc06cf07dabe52f972e7f79d8e3", null ],
    [ "fMaxRelAmp", "classpars_sd_scan.html#a95f534116aad3164fb43d0cb0015492d", null ],
    [ "fMinStep", "classpars_sd_scan.html#a1cfda46271ba28718885e743504af073", null ],
    [ "fNpScan", "classpars_sd_scan.html#a8f5db05b1390344237044fc7face82a9", null ]
];