var annotated =
[
    [ "SD", "namespace_s_d.html", null ],
    [ "clusterType", "unioncluster_type.html", "unioncluster_type" ],
    [ "compPixels", "classcomp_pixels.html", "classcomp_pixels" ],
    [ "imageStatus", "classimage_status.html", "classimage_status" ],
    [ "parsClusterFinder", "classpars_cluster_finder.html", "classpars_cluster_finder" ],
    [ "parsFillHists", "classpars_fill_hists.html", "classpars_fill_hists" ],
    [ "parsInputOutput", "classpars_input_output.html", "classpars_input_output" ],
    [ "parsPedestals", "classpars_pedestals.html", "classpars_pedestals" ],
    [ "parsSdScan", "classpars_sd_scan.html", "classpars_sd_scan" ],
    [ "parsSdThreads", "classpars_sd_threads.html", "classpars_sd_threads" ],
    [ "parsTransformation", "classpars_transformation.html", "classpars_transformation" ],
    [ "pix32", "unionpix32.html", "unionpix32" ],
    [ "pixel", "classpixel.html", "classpixel" ],
    [ "pixelColumnCompare", "classpixel_column_compare.html", "classpixel_column_compare" ],
    [ "SDAliases", "class_s_d_aliases.html", "class_s_d_aliases" ],
    [ "SDCluster", "class_s_d_cluster.html", "class_s_d_cluster" ],
    [ "SDCluster2D", "class_s_d_cluster2_d.html", "class_s_d_cluster2_d" ],
    [ "SDClusterBase", "class_s_d_cluster_base.html", "class_s_d_cluster_base" ],
    [ "sdClusterChar", "classsd_cluster_char.html", "classsd_cluster_char" ],
    [ "SDClusterFinder2DOne", "class_s_d_cluster_finder2_d_one.html", "class_s_d_cluster_finder2_d_one" ],
    [ "SDClusterFinder2DThree", "class_s_d_cluster_finder2_d_three.html", "class_s_d_cluster_finder2_d_three" ],
    [ "SDClusterFinder2DTwo", "class_s_d_cluster_finder2_d_two.html", "class_s_d_cluster_finder2_d_two" ],
    [ "SDClusterFinderBase", "class_s_d_cluster_finder_base.html", "class_s_d_cluster_finder_base" ],
    [ "SDClusterFinderStdOne", "class_s_d_cluster_finder_std_one.html", "class_s_d_cluster_finder_std_one" ],
    [ "SDClusterFinderStdTwo", "class_s_d_cluster_finder_std_two.html", "class_s_d_cluster_finder_std_two" ],
    [ "SDClusterInfo", "class_s_d_cluster_info.html", "class_s_d_cluster_info" ],
    [ "SDClusterScan", "class_s_d_cluster_scan.html", "class_s_d_cluster_scan" ],
    [ "SDClusterSplittingManager", "class_s_d_cluster_splitting_manager.html", "class_s_d_cluster_splitting_manager" ],
    [ "sdCommon", "structsd_common.html", "structsd_common" ],
    [ "SDConfigurationInfo", "class_s_d_configuration_info.html", "class_s_d_configuration_info" ],
    [ "SDConfigurationParser", "class_s_d_configuration_parser.html", "class_s_d_configuration_parser" ],
    [ "SDControlBar", "class_s_d_control_bar.html", "class_s_d_control_bar" ],
    [ "SDEllipsoid", "class_s_d_ellipsoid.html", "class_s_d_ellipsoid" ],
    [ "SDF0Base", "class_s_d_f0_base.html", "class_s_d_f0_base" ],
    [ "SDF0Uniform", "class_s_d_f0_uniform.html", "class_s_d_f0_uniform" ],
    [ "SDGui", "class_s_d_gui.html", "class_s_d_gui" ],
    [ "SDImageAnalysis", "class_s_d_image_analysis.html", "class_s_d_image_analysis" ],
    [ "SDImagesPool", "class_s_d_images_pool.html", "class_s_d_images_pool" ],
    [ "SDManager", "class_s_d_manager.html", "class_s_d_manager" ],
    [ "SDObjectSet", "class_s_d_object_set.html", "class_s_d_object_set" ],
    [ "SDPedestals", "class_s_d_pedestals.html", "class_s_d_pedestals" ],
    [ "SDPictures", "class_s_d_pictures.html", "class_s_d_pictures" ],
    [ "sdSimPars", "classsd_sim_pars.html", "classsd_sim_pars" ],
    [ "SDSimPictures", "class_s_d_sim_pictures.html", "class_s_d_sim_pictures" ],
    [ "SDSimulationManager", "class_s_d_simulation_manager.html", "class_s_d_simulation_manager" ],
    [ "SDSplittingAlgoBase", "class_s_d_splitting_algo_base.html", "class_s_d_splitting_algo_base" ],
    [ "SDSplittingAlgoWithTwoMaximumOnX", "class_s_d_splitting_algo_with_two_maximum_on_x.html", "class_s_d_splitting_algo_with_two_maximum_on_x" ],
    [ "SDTiffFileInfo", "class_s_d_tiff_file_info.html", "class_s_d_tiff_file_info" ],
    [ "SDTransformation", "class_s_d_transformation.html", "class_s_d_transformation" ],
    [ "SDTrialVersion", "class_s_d_trial_version.html", "class_s_d_trial_version" ],
    [ "SDUtils", "class_s_d_utils.html", "class_s_d_utils" ],
    [ "SimResponseFunction", "class_sim_response_function.html", "class_sim_response_function" ],
    [ "SimSimpleGauss", "class_sim_simple_gauss.html", "class_sim_simple_gauss" ],
    [ "tabRec", "structtab_rec.html", "structtab_rec" ],
    [ "tag866c", "structtag866c.html", "structtag866c" ],
    [ "tifHeader", "classtif_header.html", "classtif_header" ],
    [ "typeDecomp", "structtype_decomp.html", "structtype_decomp" ]
];