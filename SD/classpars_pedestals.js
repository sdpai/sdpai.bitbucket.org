var classpars_pedestals =
[
    [ "parsPedestals", "classpars_pedestals.html#a56a6072185e054c28260ae76bfbab8ae", null ],
    [ "~parsPedestals", "classpars_pedestals.html#a1daf4d2233a2225f46638472d4561b38", null ],
    [ "algo", "classpars_pedestals.html#ad698ceda0c68dd8765012b98e8ec9b36", null ],
    [ "chi2cut", "classpars_pedestals.html#ad89f6c66883472873f371c38237464da", null ],
    [ "nsig", "classpars_pedestals.html#ab0633e6b374a0cff9dcff93b68928b15", null ],
    [ "set", "classpars_pedestals.html#adcd905e45cfb9a4a2a959380bf723175", null ],
    [ "sigcut", "classpars_pedestals.html#ae4496287e884af7477b261ea3b4ce054", null ],
    [ "smooth", "classpars_pedestals.html#ada557c2516955bb6d1eb7b56bc2664cf", null ]
];