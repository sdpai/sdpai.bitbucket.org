var hierarchy =
[
    [ "clusterType", "unioncluster_type.html", null ],
    [ "compPixels", "classcomp_pixels.html", null ],
    [ "imageStatus", "classimage_status.html", null ],
    [ "parsClusterFinder", "classpars_cluster_finder.html", null ],
    [ "parsFillHists", "classpars_fill_hists.html", null ],
    [ "parsInputOutput", "classpars_input_output.html", null ],
    [ "parsPedestals", "classpars_pedestals.html", null ],
    [ "parsSdScan", "classpars_sd_scan.html", null ],
    [ "parsSdThreads", "classpars_sd_threads.html", null ],
    [ "parsTransformation", "classpars_transformation.html", null ],
    [ "pix32", "unionpix32.html", null ],
    [ "pixel", "classpixel.html", null ],
    [ "pixelColumnCompare", "classpixel_column_compare.html", null ],
    [ "SDClusterBase", "class_s_d_cluster_base.html", [
      [ "SDCluster", "class_s_d_cluster.html", null ],
      [ "SDCluster2D", "class_s_d_cluster2_d.html", null ]
    ] ],
    [ "sdClusterChar", "classsd_cluster_char.html", null ],
    [ "SDClusterFinderBase", "class_s_d_cluster_finder_base.html", [
      [ "SDClusterFinderStdOne", "class_s_d_cluster_finder_std_one.html", [
        [ "SDClusterFinder2DOne", "class_s_d_cluster_finder2_d_one.html", [
          [ "SDClusterFinder2DThree", "class_s_d_cluster_finder2_d_three.html", null ],
          [ "SDClusterFinder2DTwo", "class_s_d_cluster_finder2_d_two.html", null ]
        ] ],
        [ "SDClusterFinderStdTwo", "class_s_d_cluster_finder_std_two.html", null ]
      ] ]
    ] ],
    [ "sdCommon", "structsd_common.html", null ],
    [ "sdSimPars", "classsd_sim_pars.html", null ],
    [ "tabRec", "structtab_rec.html", null ],
    [ "tag866c", "structtag866c.html", null ],
    [ "TDataSet", null, [
      [ "SDCluster2D", "class_s_d_cluster2_d.html", null ],
      [ "SDConfigurationInfo", "class_s_d_configuration_info.html", null ],
      [ "SDConfigurationParser", "class_s_d_configuration_parser.html", null ],
      [ "SDControlBar", "class_s_d_control_bar.html", null ],
      [ "SDImagesPool", "class_s_d_images_pool.html", null ],
      [ "SDTrialVersion", "class_s_d_trial_version.html", null ],
      [ "SDUtils", "class_s_d_utils.html", null ]
    ] ],
    [ "TGMainFrame", null, [
      [ "SDGui", "class_s_d_gui.html", null ]
    ] ],
    [ "tifHeader", "classtif_header.html", null ],
    [ "TNamed", null, [
      [ "SDAliases", "class_s_d_aliases.html", null ]
    ] ],
    [ "TObjectSet", null, [
      [ "SDObjectSet", "class_s_d_object_set.html", [
        [ "SDClusterFinderStdOne", "class_s_d_cluster_finder_std_one.html", null ],
        [ "SDClusterInfo", "class_s_d_cluster_info.html", null ],
        [ "SDClusterScan", "class_s_d_cluster_scan.html", null ],
        [ "SDClusterSplittingManager", "class_s_d_cluster_splitting_manager.html", null ],
        [ "SDF0Base", "class_s_d_f0_base.html", [
          [ "SDF0Uniform", "class_s_d_f0_uniform.html", null ]
        ] ],
        [ "SDImageAnalysis", "class_s_d_image_analysis.html", null ],
        [ "SDManager", "class_s_d_manager.html", null ],
        [ "SDPedestals", "class_s_d_pedestals.html", null ],
        [ "SDPictures", "class_s_d_pictures.html", null ],
        [ "SDSimPictures", "class_s_d_sim_pictures.html", null ],
        [ "SDSimulationManager", "class_s_d_simulation_manager.html", null ],
        [ "SDSplittingAlgoBase", "class_s_d_splitting_algo_base.html", [
          [ "SDSplittingAlgoWithTwoMaximumOnX", "class_s_d_splitting_algo_with_two_maximum_on_x.html", null ]
        ] ],
        [ "SDTiffFileInfo", "class_s_d_tiff_file_info.html", null ],
        [ "SDTransformation", "class_s_d_transformation.html", null ],
        [ "SimResponseFunction", "class_sim_response_function.html", [
          [ "SDEllipsoid", "class_s_d_ellipsoid.html", null ],
          [ "SimSimpleGauss", "class_sim_simple_gauss.html", null ]
        ] ]
      ] ]
    ] ],
    [ "TTable", null, [
      [ "SDCluster", "class_s_d_cluster.html", null ]
    ] ],
    [ "typeDecomp", "structtype_decomp.html", null ]
];