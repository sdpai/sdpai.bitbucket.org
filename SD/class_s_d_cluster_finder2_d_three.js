var class_s_d_cluster_finder2_d_three =
[
    [ "ECLFThreadsStatus", "class_s_d_cluster_finder2_d_three.html#aef77f476545be1e4feff44de226ea12e", [
      [ "kNoThreads", "class_s_d_cluster_finder2_d_three.html#aef77f476545be1e4feff44de226ea12ea337c96fd70ea5550c8161f6101cd8c72", null ],
      [ "kTThreads", "class_s_d_cluster_finder2_d_three.html#aef77f476545be1e4feff44de226ea12ea0ae2db5bf1fc48ebf54dc1a1b84e4f82", null ],
      [ "kPosixThreads", "class_s_d_cluster_finder2_d_three.html#aef77f476545be1e4feff44de226ea12eac84ef362dd83c8a85ee8e762e6f3368e", null ],
      [ "kWindowsThreads", "class_s_d_cluster_finder2_d_three.html#aef77f476545be1e4feff44de226ea12ea892b0f9a7e461c0587ae819228dab7e1", null ]
    ] ],
    [ "SDClusterFinder2DThree", "class_s_d_cluster_finder2_d_three.html#a9f5993eb5ede242253b1cceabd1e14fe", null ],
    [ "SDClusterFinder2DThree", "class_s_d_cluster_finder2_d_three.html#a2dbfdc944e0cb730e24629d2590c2e05", null ],
    [ "~SDClusterFinder2DThree", "class_s_d_cluster_finder2_d_three.html#ad9eaffd1db0ac257a1c9473d71741472", null ],
    [ "ArePixelsNeighbours", "class_s_d_cluster_finder2_d_three.html#a0933fa2d2a7037faac04e8805887620f", null ],
    [ "BookSummaryHists", "class_s_d_cluster_finder2_d_three.html#a259b06b2950910961bc567c13cb1bbde", null ],
    [ "CalculateClusterCharachteristics", "class_s_d_cluster_finder2_d_three.html#ab3926943321caec83f351f5e89194cd2", null ],
    [ "ClearClusterFinder", "class_s_d_cluster_finder2_d_three.html#ab9bf061bd256fd8a4b858ca94d89a144", null ],
    [ "ClearSDObject", "class_s_d_cluster_finder2_d_three.html#aeaa898d5cb5fe0430576f286811fc0fa", null ],
    [ "DrawBackgroundColumns", "class_s_d_cluster_finder2_d_three.html#a2fa1c49dfb6f1946fe89b07e533e4864", null ],
    [ "DrawCFSummary", "class_s_d_cluster_finder2_d_three.html#a5e5625ee25a6262c7ec95854b722859f", null ],
    [ "DrawClusterBox", "class_s_d_cluster_finder2_d_three.html#a1eac38f8477d1d8065ba56211037546d", null ],
    [ "DrawFinalMessage", "class_s_d_cluster_finder2_d_three.html#ab82442cbbe505a9d13ccf268c6f36694", null ],
    [ "DrawInputHistInCanvas", "class_s_d_cluster_finder2_d_three.html#a5e102358586b23d2f641e096588ef3b8", null ],
    [ "DrawInputHistInCanvasWithClusters", "class_s_d_cluster_finder2_d_three.html#ade9d258ed222df4e0c9e9836368d1c74", null ],
    [ "FillRemnantHist", "class_s_d_cluster_finder2_d_three.html#a2aba012e6ba650e7393d02f6baf3a7b8", null ],
    [ "FillSummaryHists", "class_s_d_cluster_finder2_d_three.html#a85f3f4a8bcc241aa510864aa2dce1456", null ],
    [ "FindClsutersWithPosixThreads", "class_s_d_cluster_finder2_d_three.html#afe9d4fa05fc4314d70d76f5e48b184b2", null ],
    [ "FindClsutersWithThreads", "class_s_d_cluster_finder2_d_three.html#aa3202528a83ca090d6d67043dd2db363", null ],
    [ "FindClusters", "class_s_d_cluster_finder2_d_three.html#aa021b27fc8af4f3f292532911866a416", null ],
    [ "FindClustersStatic", "class_s_d_cluster_finder2_d_three.html#aac9bd989d88a0996a3046e8dedf87849", null ],
    [ "GetAmpCut", "class_s_d_cluster_finder2_d_three.html#a17921e85a5549b002a3f60c74a445dae", null ],
    [ "GetCanvas", "class_s_d_cluster_finder2_d_three.html#a703bbcaa084bd89c1c2146608982be3b", null ],
    [ "GetCLF", "class_s_d_cluster_finder2_d_three.html#ab4af8504154b5029d89534f7efc94338", null ],
    [ "GetCLFPool", "class_s_d_cluster_finder2_d_three.html#ac0d82e70fd42bdb051cd7e74322b6c5d", null ],
    [ "GetClusterInfo", "class_s_d_cluster_finder2_d_three.html#a3a53f6f7d972a3c9d4bfac6db678d2ca", null ],
    [ "GetClusters", "class_s_d_cluster_finder2_d_three.html#ae7c30c3c5744455217ee77e7032c7b3d", null ],
    [ "GetDebug", "class_s_d_cluster_finder2_d_three.html#ae5dbf39d80761962a5bcae3fc335d3ff", null ],
    [ "GetHists", "class_s_d_cluster_finder2_d_three.html#a524fd40d0e9fddc237c231291c77d8b1", null ],
    [ "GetImageAnalysis", "class_s_d_cluster_finder2_d_three.html#af2a54140955d7ea1ef55a18152c25b16", null ],
    [ "GetInputHists", "class_s_d_cluster_finder2_d_three.html#ac3723e233b3bf8e10e2c0bc7f4e8d73d", null ],
    [ "GetList", "class_s_d_cluster_finder2_d_three.html#ab8ac2a55d1ab09acf8fab2add6416c46", null ],
    [ "GetListHists", "class_s_d_cluster_finder2_d_three.html#a75e2d08c7d4532f074284d44d5a56813", null ],
    [ "GetListHists", "class_s_d_cluster_finder2_d_three.html#a37518e57f827e2d22fcefc246d78463e", null ],
    [ "GetListHistsMain", "class_s_d_cluster_finder2_d_three.html#a24c55b72d995f5a99e3a0c573ad250a0", null ],
    [ "GetListHistsSum", "class_s_d_cluster_finder2_d_three.html#adb851027e7254f0df0c97fdf21843f11", null ],
    [ "GetListOfBgLines", "class_s_d_cluster_finder2_d_three.html#a30132159230e74bf92573a8d9dcabf4e", null ],
    [ "GetMainHists", "class_s_d_cluster_finder2_d_three.html#af13fb3443d55cbaddbda5cb0adde6aa8", null ],
    [ "GetNClstsAll", "class_s_d_cluster_finder2_d_three.html#a5dd208830439be8745e0c90a6e48ddd1", null ],
    [ "GetOnlineDrawing", "class_s_d_cluster_finder2_d_three.html#ac9a5eb84f9aa33515eb172e2a610ea08", null ],
    [ "GetPars", "class_s_d_cluster_finder2_d_three.html#ab0f61ee082af4969b3819fa7ad3fb3a6", null ],
    [ "GetPedestal", "class_s_d_cluster_finder2_d_three.html#aba82d06f6964c758f24d7b28ba394355", null ],
    [ "GetSDManager", "class_s_d_cluster_finder2_d_three.html#a53dd05397370dfee585c3efb33144e1a", null ],
    [ "GetSeed", "class_s_d_cluster_finder2_d_three.html#a51296e29e9c0222f25bc3fbd278dd6a9", null ],
    [ "GetSignature", "class_s_d_cluster_finder2_d_three.html#aae82cc15948f62b19b28c3edb7433999", null ],
    [ "GetSummaryHists", "class_s_d_cluster_finder2_d_three.html#aeb48adf563bc3bffe695eb9ce0aa92e7", null ],
    [ "GetThreadsStatus", "class_s_d_cluster_finder2_d_three.html#a8a22c03816c6963ecfa996bd53044abe", null ],
    [ "Init", "class_s_d_cluster_finder2_d_three.html#a10c012984f68f06415471d67b8559b76", null ],
    [ "InitHists", "class_s_d_cluster_finder2_d_three.html#a42d9b2ea8d71b29db24043d040dc76da", null ],
    [ "IsPixelNeighbourToCluster", "class_s_d_cluster_finder2_d_three.html#ab5ddb03c02355761253073040f21e692", null ],
    [ "IsThisClusterReal", "class_s_d_cluster_finder2_d_three.html#a7723606ee80e1d02a93491f3c32546ad", null ],
    [ "IsThreadsAvailable", "class_s_d_cluster_finder2_d_three.html#a9290c9df1d7e739f8ed2c6005ad1f7c7", null ],
    [ "MergeClusterFinders", "class_s_d_cluster_finder2_d_three.html#ad5aa763456c08491c302637e5b65baa7", null ],
    [ "PrintClusterScan", "class_s_d_cluster_finder2_d_three.html#af3361dfcb0b30ae403f2f394b158bbab", null ],
    [ "PrintClusterScanCSV", "class_s_d_cluster_finder2_d_three.html#a236eb2a824603779fd1f85336b8a3485", null ],
    [ "PrintRecord", "class_s_d_cluster_finder2_d_three.html#a9915a8ed14ed26c4512acfb533e7acb2", null ],
    [ "PrintRecordCSV", "class_s_d_cluster_finder2_d_three.html#a6813c7cd40a8ab35b06c842900c30b39", null ],
    [ "SaveTxtFile", "class_s_d_cluster_finder2_d_three.html#ad9e700c74715153530d2c58e53edd5fb", null ],
    [ "SetAmpCut", "class_s_d_cluster_finder2_d_three.html#ab661d3367e78bf28a39c48db0b7b9c0d", null ],
    [ "SetCFName", "class_s_d_cluster_finder2_d_three.html#a511757f596391e44eb11e073770c0f0d", null ],
    [ "SetCFPars", "class_s_d_cluster_finder2_d_three.html#a82c82217c89b162b79e6ee2637410b94", null ],
    [ "SetDebug", "class_s_d_cluster_finder2_d_three.html#a8c766b4767d01d74a24109f8ccb1f226", null ],
    [ "SetInputHist", "class_s_d_cluster_finder2_d_three.html#abc096b3df543a43f5548a8b4c7561327", null ],
    [ "SetOnlineDrawing", "class_s_d_cluster_finder2_d_three.html#a523949121025e70b6cc2a578e98e39d2", null ],
    [ "SetSeed", "class_s_d_cluster_finder2_d_three.html#aab8214dd4441cd53a6113e4c0fcf5fa6", null ],
    [ "SetThreadsStatus", "class_s_d_cluster_finder2_d_three.html#a724b59d486286bb75c773f66fb24588c", null ],
    [ "SortAndRenameClusters", "class_s_d_cluster_finder2_d_three.html#af0c78ee2f201b620d0bf4e4c3d580f64", null ],
    [ "fAmpCut", "class_s_d_cluster_finder2_d_three.html#a022de33faa69fb8026e16ebf24757d47", null ],
    [ "fC", "class_s_d_cluster_finder2_d_three.html#a425fc8a4a2999824b9ad387e32ec0972", null ],
    [ "fDebug", "class_s_d_cluster_finder2_d_three.html#a73622faae9439e5a4428b3bbea367109", null ],
    [ "fHInCopy", "class_s_d_cluster_finder2_d_three.html#a4c288093b423ffff14cbe2be129b05d5", null ],
    [ "fNClstsAll", "class_s_d_cluster_finder2_d_three.html#a304411d43e998e3240badbe4a431feda", null ],
    [ "fOnlineDrawing", "class_s_d_cluster_finder2_d_three.html#afe9ecb4a68ef5ab35c32343a4b5a6824", null ],
    [ "fPars", "class_s_d_cluster_finder2_d_three.html#aeab16b9f3c5d217453f4577e7d004f38", null ],
    [ "fSignature", "class_s_d_cluster_finder2_d_three.html#a02b97068d2ddfc8c3162035f29d76109", null ],
    [ "fThreadsStatus", "class_s_d_cluster_finder2_d_three.html#a81de96a8c21f869ab258efa59296e0cf", null ],
    [ "gkBgLines", "class_s_d_cluster_finder2_d_three.html#a8e22de3a4c6a47b5d7bea829ff372ebf", null ],
    [ "gkCLFPool", "class_s_d_cluster_finder2_d_three.html#aed6d9425c93a8a7bb5de4654f0041e43", null ],
    [ "gkNameHistsCFSum", "class_s_d_cluster_finder2_d_three.html#a6cb60e162c78a62826b693cf73e27951", null ],
    [ "gkNameHistsMain", "class_s_d_cluster_finder2_d_three.html#a43c0e7d0d14b77636b6f4564832ece1c", null ],
    [ "gkNameInputHists", "class_s_d_cluster_finder2_d_three.html#adf79cd76fde09bae06c40ed7585b2c7b", null ]
];